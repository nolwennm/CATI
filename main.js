//LIST OF TODOS
// -propagate textbox changes to the following ones
// -factor in the score of the skeleton keypoints to display only the robust ones
// -remove dependancy to google server for posenet
// -take into account the size of the skeleton for the constraint (owl on the shoulder size depends on the size of the person)

//OPEN QUESTIONS
// - should we only be able to create constraints in tableau0?

var store = {
	//this is where we keep track of all global states
	debug: true,
	state: {
		isSelectMode: false,
		isDragging: false,
		isAttachMode: false,
		isTrackingPose: false,
		isActiveConstraint: false,
		tableauCounter: 0,
		baselineCounter: 0,
		images: [
		{
			adressImage: 'assets/pose-1.jpg',
		},
		{
			adressImage: 'assets/pose-2.jpg',
		},
		],
		cursorPos: {
			posX: undefined,
			posY: undefined,
		}
	},
}

Vue.component('main-canvas', {
	template:`
	<div id="inner-app" 
		@mousemove="cursorMoved" 
		:class="{ appAttachMode: sharedState.isAttachMode }">
		<my-menu></my-menu>
		<div id="main-canvas">
			<tableau id="tableau0" ref="tableau0"></tableau>
			<tableau id="tableau1" ref="tableau1" @updateConstraint="updateConstraint"></tableau>
		</div>
	</div>
	`,
	data(){
		return{
			sharedState: store.state,
		}
	},
	computed: {
	},
	methods: {
		cursorMoved: function(event){
			//here we always store the up to date cursor position in the store
			this.sharedState.cursorPos.posX = event.pageX;
			this.sharedState.cursorPos.posY = event.pageY;
		},
		cloneBaseline(initialBaseline){
			console.log('im in cloneBaseline in the main-canvas');
			for(i=1; i<this.sharedState.tableauCounter; i++){
				var tableauTo = this.$refs['tableau'+i];
				var tableauInitial = this.$refs['tableau'+0];

				tableauTo.baselines.push(
					{
					id: initialBaseline.id,
					personindex: initialBaseline.personindex,
					points: [],
					}
				);
				for(i=0; i<initialBaseline.points.length; i++){
					//we get the equivalent keypoint in tableauTo
					var refName = 'person'+initialBaseline.personindex+initialBaseline.points[i];
					var keypointTo = tableauTo.$refs[refName][0];
					tableauTo.baselines[tableauTo.baselines.length-1].points.push(
						keypointTo
					);
				};
				//we are telling the baseline in tableauTo to activate itself
				console.log(initialBaseline.id);
				console.log(tableauTo.$refs['baseline0'][0]);
				tableauTo.$refs['baseline'+initialBaseline.id][0].isActiveBaseline = initialBaseline.id;
			};
		},
		cloneConstraint(initialConstraint){
			for(i=1; i<this.sharedState.tableauCounter; i++){
				//I'm getting the id in the initial tableau to fetch the same element in another tableau
				//TOFIX this is terrible but haven't found a better way, sorry future Nolwenn
				var tableauTo = this.$refs['tableau'+i];
				var tableauInitial = this.$refs['tableau'+0];

				var imagefeatureTo = tableauTo.$refs[initialConstraint.from.$el.id][0];
				var controlTo = tableauTo.$refs[initialConstraint.to.$parent.$el.id][0].$refs[initialConstraint.to.$el.id][0];
				tableauTo.constraints.push(
					{
					from: imagefeatureTo,
					to: controlTo,
					initialFrom: initialConstraint.from,
					initialTo: initialConstraint.to, 
					}
				);
				//we are telling the textbox that it should be ready to update its position according to this constraint
				controlTo.$parent.isActiveConstraint = tableauTo.constraints.length-1;
			}
		},
		updateConstraint(imagefeature){
			//triggered when we are updating the image features
			//we should update the constraint only once, if/when the new imagefeature was already in use in a constraint
			for (i=0; i<this.$refs.tableau0.constraints.length; i++){
				if(this.$refs.tableau0.constraints[i].from.$el.id == imagefeature.$el.id ){
					this.cloneConstraint(this.$refs.tableau0.constraints[i]);
				};
			};
		},
	},
	mounted: function () {
	},
})

Vue.component('tableau', {
	template:`
	<div 
		:id="'tableau'+this.id"  
		:class="{ tracking: isTracking }" class="tableau"
		@click="tableauClicked">
		<div class="img-container" contenteditable="true">
			<img 
				contenteditable="false"
				:id="'img'+this.id" class="img" 
				:src="this.sharedState.images[this.id || 0].adressImage"
			/>
			<svg :id="'canvasSVG'+this.id" class="canvasSVG">
				<constraint
					v-for="constraint in constraints" 
					:key="constraint.id" 
					:constraint=constraint 
					:ref="'constraint'+constraint.id"
					:class="{ constraintAttachMode: sharedState.isAttachMode }">
				</constraint>
				<skeleton 
					v-for="(person, index) in imagefeatures[0]"
					:key="'skeleton'+index"
					:id="'skeleton'+index"
					:person=person
					:personindex=index
					:tableauindex=id
					@createBaseline="createBaseline">
				</skeleton> 
				<baseline
					v-for="(baseline, index) in baselines"
					:key="'baseline'+index"
					:ref="'baseline'+index"
					:id="'baseline'+index"
					:baseline=baseline
					:baselineindex=index>
				</baseline>
			</svg>
			<div 
				v-for="imagefeature in imagefeatures"
				class="imgFeaturesContainer">
				<div
					v-for="(person, index) in imagefeature"
					class="person"
					:id="'person'+index">
						<imagefeature 
							v-for="keypoint in person.keypoints"
							:keypoint=keypoint
							:key="'person'+index+keypoint.part" 
							:ref="'person'+index+keypoint.part"
							:id="'person'+index+keypoint.part"
							@updateConstraint="updateConstraint">
						</imagefeature> 
				</div>
			</div>
			<textbox 
				v-for="(textbox, index) in textboxes"
				:key="'textbox'+ index"
				:id="'textbox'+ index"
				:constraints=constraints 
				:textbox=textbox
				:class="{ textboxIsSelected: this.isSelected }"
				:ref="'textbox'+ index">
			</textbox>
		</div>
	</div>
	`,
	data(){
		return {
			id: undefined,
			isVisible: false,
			isAttaching: false,
			isTracking: false,
			isCreatingBaseline: false,
			sharedState: store.state,
			//imagefeature key-values: person[ score:, keypoints[position.x, position.y]],
			imagefeatures: [],
			//constraint key-values: id:, from:, to:, distancex:, distanceY:,
			constraints: [],
			baselines: [],
			//textbox key-value: text:,
			textboxes: [
				{text: '🌺'},
				{text: '🦉'},
			],
		}
	},
	methods: {
		tableauClicked(e){
			if(this.isAttaching == true){
				//if we were attaching but clicked on nowhere, we call stopCreatingConstraint
				this.stopCreatingConstraint(this.constraints.length-1, e);
			} else if(this.sharedState.isSelectMode == true){
				//if some elements were being selected, we want to deselect them
				//TODO should this be at the main-canvas level?
				for(i=0; i<this.$children.length; i++){
					this.$children[i].isSelected = false;
					this.$children[i].isDragging = false;
				}
				this.sharedState.isSelectMode = false;
			}

		},
		updateConstraint(imagefeature){
			this.$emit('updateConstraint', imagefeature);
		},
		createBaseline(line){
			this.sharedState.baselineCounter ++;		
			if(this.isCreatingBaseline == true){
				this.baselines[this.baselines.length-1].points.push(
					line.bone.pointB.part, line.bone.pointA.part
				);
			}
			else{
				this.baselines.push(
					{
					id: this.baselines.length,
					personindex: line.personindex,
					points: [line.bone.pointB.part, line.bone.pointA.part],
					}
				);
				this.isCreatingBaseline = true;	
			}
			//we then clone it in the other tableaux
			//this.$parent.cloneBaseline(this.baselines[this.baselines.length-1]);
		},
		creatingConstraint(clickedElement, e){
			if(this.sharedState.isAttachMode == true && this.isAttaching == false){
				//we try to create a new constraint, temporary attaching it to the mouse cursor;
				var newConstraintId = this.constraints.length;
				//TOFIX for now, we want the imagefeature to be the 'from' element
				if(clickedElement.$el.classList.contains('control')){
					this.constraints.push(
					{
						id: newConstraintId,
						from: this.sharedState.cursorPos,
						to: clickedElement,
						distanceX: undefined,
						distanceY: undefined,
					}
					);
				}
				else{
					this.constraints.push(
					{
						id: newConstraintId,
						from: clickedElement,
						to: this.sharedState.cursorPos,
						distanceX: undefined,
						distanceY: undefined,
					}
					);
				}
				this.isAttaching = true;
			}
			else if(this.sharedState.isAttachMode == true && this.isAttaching == true){
				//we are attaching the constraint on an object
				if(clickedElement.$el.classList.contains('control')){
					Vue.set(this.constraints[this.constraints.length-1], 'to', clickedElement );
				}
				else{
					Vue.set(this.constraints[this.constraints.length-1], 'from', clickedElement );
				}

				this.isAttaching = false;
				this.isAttachMode = false;

				this.sharedState.isActiveConstraint = true;

				//TODO we emit the change to create constraints on the other tableaux
				this.$parent.cloneConstraint(this.constraints[this.constraints.length-1]);
			}
		},
		stopCreatingConstraint(currentConstraint, event){
			this.$delete(this.constraints, currentConstraint);
			this.isAttaching = false;
		},
		cleanImagefeaturesAndConstraints() {
			if(this.constraints.length != 0){
				for(i=this.constraints.length; i>=0; i--){
					Vue.delete(this.constraints, i);
				};
			}
			for(i=this.imagefeatures.length; i>=0; i--){
				Vue.delete(this.imagefeatures, i);
			};
		},
		trackingPose() {
			//we signal to the user that we started tracking
			this.isTracking = true;
			//first, we should clean the existing imagefeatures and constraints
			if(this.imagefeatures.length != 0){
				this.cleanImagefeaturesAndConstraints();
			};
			//Using tensorflow pretrained posenet model
			//multiple person detection, takes longer
			var tableau = this;
			var imageScaleFactor = 0.5;
			var flipHorizontal = false;
			var outputStride = 16;
			var maxPoseDetections= 10;

			var imgId = 'img' + String(this.id);
			var image = document.getElementById(imgId);

			posenet.load().then(function(net){
				return net.estimateMultiplePoses(image, 0.5, flipHorizontal, outputStride, maxPoseDetections)
			}).then(function(poses){
		    	//we store the values as new imagefeatures
		    	tableau.imagefeatures.push(poses);
		    	tableau.isTracking = false;
		    	//path in the imagefeatures:
		    	//console.log(tableau.imagefeatures[0][0].keypoints[0].position.x);
		    });


		    /*single person detection
		    var imageScaleFactor = 0.5;
		    var outputStride = 16;
		    var flipHorizontal = false;
		 
		    var imageElement = document.getElementById('cat');
		 
		    posenet.load().then(function(net){
		      return net.estimateSinglePose(imageElement, imageScaleFactor, flipHorizontal, outputStride)
		    }).then(function(pose){
		      console.log(pose);
		  })*/
		},
		trackingEyes() {
			//NOT IN USE for now
			//this function is using the tracking.js library
			var tableau = this;
			//first, we should clean the existing imagefeatures and constraints
			if(tableau.imagefeatures.length != 0){
				this.cleanImagefeaturesAndConstraints();
			};
			var imgId = '#img' + String(this.id);
			var tracker = new tracking.ObjectTracker(['eye']);

			tracker.setStepSize(1.7);
			tracking.track(imgId, tracker);
	        //create a rectangle for each tracked eye
	        tracker.on('track', function(event,) {
	        	event.data.forEach(function(rect, index) {
	        		window.plot(rect.x, rect.y, rect.width, rect.height, index, tableau);
	        	});
	        });
	        //add new imagefeature object in the list
	        window.plot = function(x, y, w, h, i, tableau) {
	        	var newImagefeatureId = tableau.imagefeatures.length;
	        	tableau.imagefeatures.push(
	        	{
	        		id: newImagefeatureId,
	        		left: x + (w/2),
	        		top: y + (h/2),
	        	}
	        	);
	        };
	    },
	    trackingMouth() {
    		//NOT IN USE for now
    		var tableau = this;
			//first, we should clean the existing imagefeatures and constraints
			if(tableau.imagefeatures.length != 0){
				for(i=tableau.imagefeatures.length; i>=0; i--){
					Vue.delete(tableau.imagefeatures, i);
				};
				for(i=tableau.constraints.length; i>=0; i--){
					Vue.delete(tableau.constraints, i);
				};
				
			};
			var imgId = '#img' + String(this.id);
			var tracker = new tracking.ObjectTracker(['mouth']);

			tracker.setStepSize(1.7);
			tracking.track(imgId, tracker);
	        //create a rectangle for each tracked eye
	        tracker.on('track', function(event,) {
	        	event.data.forEach(function(rect, index) {
	        		window.plot(rect.x, rect.y, rect.width, rect.height, index, tableau);
	        	});
	        });
	        //add new imagefeature object in the list
	        window.plot = function(x, y, w, h, i, tableau) {
	        	var newImagefeatureId = tableau.imagefeatures.length;
	        	tableau.imagefeatures.push(
	        	{
	        		id: newImagefeatureId,
	        		left: x + (w/2),
	        		top: y + (h/2),
	        	}
	        	);
	        };
	    },
	},
	mounted: function () {
		this.id = this.sharedState.tableauCounter;
		this.sharedState.tableauCounter ++;
		//we launch the trackingPose when the image is hopefully loaded
		this.$nextTick(function () {
				//setTimeout(this.trackingEyes, 300);
				//setTimeout(this.trackingPose, 300);
			})
	},
})


Vue.component('constraint', {
	template:`
	<line 
		:x1="x1" 
		:y1="y1" 
		:x2="x2" 
		:y2="y2" 
		class="constraint" 
		v-if="isVisible" 
	/>
	`,
	props: ['constraint'],
	data(){
		return {
			isActive: false,
			isVisible: true,
			sharedState: store.state,
		}
	},
	computed: {
		x1: function(){
			//in case we are currently drawing the constraint, attach to the mousePosition
			if(this.constraint.from != this.sharedState.cursorPos){
				var refObject = this.constraint.from;
				return refObject.posX + 'px'
			}
			else{
				var tableauOffsetLeft = this.$parent.$el.offsetLeft + this.$parent.$el.offsetParent.offsetLeft;
				return this.sharedState.cursorPos.posX - tableauOffsetLeft + 'px'
			}
		},
		y1: function(){
			if(this.constraint.from != this.sharedState.cursorPos){
				var refObject = this.constraint.from;
				return refObject.posY + 'px'
			}
			else{
				var tableauOffsetTop = this.$parent.$el.offsetTop + this.$parent.$el.offsetParent.offsetTop;
				return this.sharedState.cursorPos.posY - tableauOffsetTop + 'px'
			}
		},
		x2: function(){
			if(this.constraint.to != this.sharedState.cursorPos){
				var refObject = this.constraint.to;
				return refObject.posX + 'px'
			}
			else{
				var tableauOffsetLeft = this.$parent.$el.offsetLeft + this.$parent.$el.offsetParent.offsetLeft;
				return this.sharedState.cursorPos.posX - tableauOffsetLeft + 'px'
			}
		},
		y2: function(){
			if(this.constraint.to != this.sharedState.cursorPos){
				var refObject = this.constraint.to;
				return refObject.posY + 'px'
			}
			else{
				var tableauOffsetTop = this.$parent.$el.offsetTop + this.$parent.$el.offsetParent.offsetTop;
				return this.sharedState.cursorPos.posY - tableauOffsetTop + 'px'
			}
		},
	}
})

var app = new Vue({
	el: '#app',
})