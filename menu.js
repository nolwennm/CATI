Vue.component('my-menu', {
	template:`
	<div class="menu">
		<div class="title section">
			<h2>CATI</h2>
			<h5>Content Aware Template</h5>
		</div>
		<div class="section">
			<toggle-attach-mode></toggle-attach-mode>
		</div>
		<div class="toolbar section">
			<button 
				class="toolbar-button" 
				id="newTextBox" 
				v-on:click.stop="createNewTextbox()">
					T
			</button>
			<button 
				class="toolbar-button" 
				id="newShape">
					◻️
			</button>
		</div>
		<div class="section">
			<h3>Tracking</h3>
			<button
				id="trackingPose"  
				:class="{ trackingison: sharedState.isTrackingPose }"
				v-on:click.stop="launchingTrackingPose()">
					Body
			</button>
			<button 
				id="trackingEyes">
					Something Else
			</button>
		</div>
		<div class="updateImage">
			<label for="file" class="label-file">Pick an Image</label>
			<input id="file" class="input-file" @change="previewImage" type="file" name="pic" accept="image/*">
		</div>
	</div>
	`,
	data(){
		return{
			sharedState: store.state,
		}
	},
	methods: {
		previewImage: function(event) {
			//TOFIX generalize it, for now works only with tableau1
			var input = event.target;
			if (input.files && input.files[0]){
				var reader = new FileReader();
				reader.onload = (e) => {
					this.sharedState.images[1].adressImage = e.target.result;
					if(this.sharedState.isActiveConstraint == true){
						setTimeout(this.$parent.$refs.tableau1.trackingPose, 200);
					};
				}
				reader.readAsDataURL(input.files[0]);
			}
		},
		launchingTrackingPose(){
			for(i=0; i<this.sharedState.tableauCounter; i++){
				var tableau = this.$parent.$refs['tableau'+i];
				setTimeout(tableau.trackingPose, 50);
				this.sharedState.isTrackingPose = true;
			};
		},
		createNewTextbox(){
			for(i=0; i<this.sharedState.tableauCounter; i++){
				var tableau = this.$parent.$refs['tableau'+i];
				tableau.textboxes.push(
				{
					text: 'new',
				}
				);
			};
		},
	}
})


Vue.component('toggle-attach-mode', {
	template:`
	<button 
		id="attachMode" 
		v-on:click.stop="toggleAttachMode()" 
		v-bind:class="{ attachMode: sharedState.isAttachMode }">
		Attach Mode
	</button>
	`,
	data(){
		return {
			sharedState: store.state,
		}
	},
	methods: {
		toggleAttachMode(){
			if(this.sharedState.isAttachMode == true){
				this.sharedState.isAttachMode = false;
			}
			else{
				this.sharedState.isAttachMode = true;
			}
		}
	},
})