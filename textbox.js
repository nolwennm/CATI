Vue.component('textbox', {
	template:`
	<div
		v-on:mousedown="startDragging" 
		v-on:click.stop="setSelect" 
		:id="id"
		:style="styleObject" 
		:contenteditable="isSelected"
		class="textbox"
		:class="{ textboxIsSelected: isSelected }">
		<div
			class="titre">
			{{textbox.text}}
		</div>
		<control 
			v-for="control in controls" 
			:ref="'control' +control.id"
			:key="control.id" 
			:id="'control' +control.id" 
			:class="control.id"
			:isSelected=isSelected >
		</control>
	</div>
	`,
	props: ['constraints', 'id', 'textbox'],
	data(){
		return {
			x: 0,
			y: 0,
			width: 50,
			height: 50,
			initialOffsetX: 0,
			initialOffsetY: 0,
			initialPosX: 0,
			initialPosY: 0,
			initialWidth: 0,
			initialHeight: 0,
			isSelected: false,
			isActiveConstraint: undefined,
			isDragging: false,
			sharedState: store.state,
			controls: [
			{id: 'tl'},
			{id: 'tr'},
			{id: 'bl'},
			{id: 'br'},
			{id: 'tc'},
			{id: 'bc'},
			{id: 'cl'},
			{id: 'cr'},
			],
		}
	},
	computed: {
		styleObject: function() {
			return {
				'left': this.posX + 'px',
				'top': this.posY + 'px',
				'width': this.width + 'px',
				'height': this.height + 'px',
				'line-height': this.height + 'px',
			}
		},
		posX: function() {
			if((this.isActiveConstraint != undefined) && (this.constraints[this.isActiveConstraint])) {
				var distanceX = this.constraints[this.isActiveConstraint].initialTo.$parent.posX - this.constraints[this.isActiveConstraint].initialFrom.posX
				var imageFeature = this.constraints[this.isActiveConstraint].from;
				return imageFeature.posX + distanceX;
			} 
			else{
				//we use this value when we are not in a constraint
				//TODO check if there is a way to avoid using this.x
				return this.x
			}
		},
		posY: function() {
			if((this.isActiveConstraint != undefined) && (this.constraints[this.isActiveConstraint])) {
				var distanceY = this.constraints[this.isActiveConstraint].initialTo.$parent.posY - this.constraints[this.isActiveConstraint].initialFrom.posY
				var refObject = this.constraints[this.isActiveConstraint].from;
				return refObject.posY + distanceY;
			} 
			else{
				return this.y
			}
		},
	},
	methods: {
		setSelect(){
			this.isSelected = true;
			this.sharedState.isSelectMode = true;
		},
		dragging(){
	        //we update the text box position according to the cursor
	        this.x = event.pageX - this.initialOffsetX;
	        this.y = event.pageY - this.initialOffsetY;

	        //we send the info to the main-canvas to syncconstraint 
	        if(this.sharedState.isActiveConstraint == true){
	        	this.$emit('draggingtextbox', this.posX, this.posY);
	        }
	        event.stopPropagation();
	    },
	    resizing(handle){
	    	if (handle.classList.contains('tl') || handle.classList.contains('bl') || handle.classList.contains('cl')) {
	    		this.posX = event.pageX - this.initialOffsetX;
	    		this.width = Math.abs(event.pageX - ((this.initialPosX + this.initialOffsetX) + this.initialWidth));
	    	};
	    	if (handle.classList.contains('tl') || handle.classList.contains('tr') || handle.classList.contains('tc')) {
	    		this.posY = event.pageY - this.initialOffsetY;
	    		this.height = Math.abs(event.pageY - ((this.initialPosY + this.initialOffsetY) + this.initialHeight));
	    	};

	    	if (handle.classList.contains('tr') || handle.classList.contains('br') || handle.classList.contains('cr')) {
	    		this.width = Math.abs(event.pageX - (this.initialPosX + this.initialOffsetX) + this.initialWidth);
	    	};
	    	if (handle.classList.contains('bl') || handle.classList.contains('bc') || handle.classList.contains('br')) {
	    		this.height = Math.abs(event.pageY - (this.initialPosY + this.initialOffsetY) + this.initialHeight);
	    	};
	    	textFit(document.getElementById('titre0'));
			//we need to update the constraint
			this.$emit('draggingtextbox', this.posX, this.posY);
		},
		applyConstraint(){
			if(this.sharedState.isActiveConstraint == true){

				var refObject = this.constraints[0].from;
				this.posX = refObject.posX + this.constraints[0].distanceX;
				this.posY = refObject.posY + this.constraints[0].distanceY;
			}
			else{
				console.log('this shouldnt happen');
			}
		},
		startDragging(){
			//we select the textbox
			this.setSelect();
			//we store the initial offset
			this.initialOffsetX = event.pageX - this.posX;
			this.initialOffsetY = event.pageY - this.posY;
			//we add the event listeners on the window
			var dragHandler;
			dragHandler = function(e) {
				this.dragging();
			}.bind(this);
			window.addEventListener('mousemove', dragHandler);

			var stopDragHandler;
			stopDragHandler = function(e) {
				window.removeEventListener('mousemove', dragHandler, false);
				window.removeEventListener('mouseup', stopDragHandler, false);
			}.bind(this)
			window.addEventListener('mouseup', stopDragHandler, false);
		},
		creatingConstraint(control, event){
			if(this.sharedState.isAttachMode == true){
				this.$parent.creatingConstraint(control, event);
			}
		},
	},
})

Vue.component('control', {
	template:`
	<div 
		class="control" 
		v-on:mousedown.stop="controlClicked($event)"
		v-on:click.stop="creatingConstraint($event)" 
		:class="{ controlAttachMode: sharedState.isAttachMode, controlResizeMode: isSelected }">
	</div>
	`,
	props: ['isSelected'],
	data(){
		return {
			sharedState: store.state,
		}
	},
	computed: {
		//here we calculate our relative position for the constraint
		//TOFIX avoid using $el and pass the class as a prop instead
		posX: function(){
			if (this.$el.classList.contains('tl') || this.$el.classList.contains('bl') || this.$el.classList.contains('cl')) {
				return this.$parent.posX
			}
			else if (this.$el.classList.contains('tr') || this.$el.classList.contains('br') || this.$el.classList.contains('cr')) {
				return +this.$parent.posX + +this.$parent.width
			}
			else if (this.$el.classList.contains('tc') || this.$el.classList.contains('bc')) {
				return +this.$parent.posX + (+this.$parent.width/2)
			}
			else{
				console.log("this shouldnt happen in control")
			}
		},
		posY: function(){
			if (this.$el.classList.contains('tl') || this.$el.classList.contains('tr') || this.$el.classList.contains('tc')) {
				return this.$parent.posY
			}
			else if (this.$el.classList.contains('bl') || this.$el.classList.contains('br') || this.$el.classList.contains('bc')) {
				//we are using unary plus to turn strings into numbers
				return +this.$parent.posY + +this.$parent.height
			}
			else if (this.$el.classList.contains('cl') || this.$el.classList.contains('cr')){
				return +this.$parent.posY + (+this.$parent.height/2)
			}
			else{
				console.log("this shouldnt happen in control")
			}

		}
	},
	methods: {
		controlClicked(event){
			if(this.isSelected == true){
				this.startResizing(event);
			}
		},
		creatingConstraint(event){
			if(this.sharedState.isAttachMode == true){
				this.$parent.creatingConstraint(this, event);
				event.stopPropagation();
			}
		},
		startResizing(e){
			//we store the initial values in the parent textbox
			this.$parent.initialPosX = this.$parent.posX;
			this.$parent.initialPosY = this.$parent.posY;
			this.$parent.initialWidth = this.$parent.width;
			this.$parent.initialHeight = this.$parent.height;
			this.$parent.initialOffsetX = e.pageX - this.$parent.posX;
			this.$parent.initialOffsetY = e.pageY - this.$parent.posY;
			//we add the event listeners on the window
			var handle = e.target;
			var resizeHandler;
			resizeHandler = function(e) {
				this.$parent.resizing(handle);
			}.bind(this);
			window.addEventListener('mousemove', resizeHandler);

			var stopResizeHandler;
			stopResizeHandler = function(e) {
				window.removeEventListener('mousemove', resizeHandler, false);
				window.removeEventListener('mouseup', stopResizeHandler, false);
			}.bind(this)
			window.addEventListener('mouseup', stopResizeHandler, false);
		},
	},
})