Vue.component('imagefeature', {
	template:`
	<div 
		class="object imagefeature" 
		v-on:click.stop="creatingConstraint($event)" 
		:style="styleObject">
	</div>
	`,
	props: ['keypoint'],
	data(){
		return{
			isVisible: false,
			sharedState: store.state,
			diameter: '8',
		}
	},
	computed: {
		posX: function(){
			return this.keypoint.position.x;
		},
		posY: function(){
			return this.keypoint.position.y;
		},
		styleObject: function() {
			return {
				'left': (this.posX - (this.diameter/2)) + 'px',
				'top': (this.posY - (this.diameter/2)) + 'px',
				'width': this.diameter + 'px',
				'height': this.diameter + 'px',
			}
		},
	},
	methods: {
		creatingConstraint(event){
			if(this.sharedState.isAttachMode == true){
				this.$parent.creatingConstraint(this, event);
				event.stopPropagation();
			}
		}
	},
	mounted: function () {
		//call main-canvas to re-clone the constraint
		if(this.sharedState.isActiveConstraint){
			this.$emit('updateConstraint', this);
		}
	},
})

Vue.component('skeleton', {
	template:`
	<svg class="skeleton">
		<bone
			v-for="(bone, i) in bones"
			:bone=bone
			:personindex=personindex
			:tableauindex=tableauindex
			:key="bone.id"
			@createBaseline="createBaseline">
		</bone>
	</svg>
	`,
	props: ['person', 'personindex', 'tableauindex'],
	data(){
		return {
			sharedState: store.state,
			bones: [
			{id: 'leftShoulder-rightShoulder', pointA: this.person.keypoints[5], pointB: this.person.keypoints[6]},
			{id: 'leftHip-rightHip', pointA: this.person.keypoints[11], pointB: this.person.keypoints[12]},
			{id: 'leftShoulder-leftElbow', pointA: this.person.keypoints[5], pointB: this.person.keypoints[7]},
			{id: 'rightShoulder-rightElbow', pointA: this.person.keypoints[6], pointB: this.person.keypoints[8]},
			{id: 'leftShoulder-leftHip', pointA: this.person.keypoints[5], pointB: this.person.keypoints[11]},
			{id: 'rightShoulder-rightHip', pointA: this.person.keypoints[6], pointB: this.person.keypoints[12]},
			{id: 'leftElbow-leftWrist', pointA: this.person.keypoints[7], pointB: this.person.keypoints[9]},
			{id: 'rightElbow-rightWrist', pointA: this.person.keypoints[8], pointB: this.person.keypoints[10]},
			{id: 'leftHip-leftKnee', pointA: this.person.keypoints[11], pointB: this.person.keypoints[13]},
			{id: 'rightHip-rightKnee', pointA: this.person.keypoints[12], pointB: this.person.keypoints[14]},
			{id: 'leftKnee-leftAnkle', pointA: this.person.keypoints[13], pointB: this.person.keypoints[15]},
			{id: 'rightKnee-rightAnkle', pointA: this.person.keypoints[14], pointB: this.person.keypoints[16]},
			],
		}
	},
	methods: {
		createBaseline(bone){
			console.log('im in createBaseline in skeleton');
			this.$emit('createBaseline', bone);
		},
	},
})

Vue.component('bone', {
	template:`
		<line 
			:id="'tableau'+tableauindex+'person'+personindex+bone.id"
			:style="styleObject"
			v-on:click.stop="displayTextpath($event)"
			:x1="pointA.x" 
			:y1="pointA.y" 
			:x2="pointB.x" 
			:y2="pointB.y" 
			class="bone"
		/>
	`,
	props: ['bone', 'tableauindex', 'personindex'],
	data(){
		return {
			sharedState: store.state,
		}
	},
	computed: {
		pointA: function(){
			var point = {
			    x:  this.bone.pointB.position.x,
			    y: this.bone.pointB.position.y,
			};
			return point;
		},
		pointB: function(){
			var point = {
			    x:  this.bone.pointA.position.x,
			    y: this.bone.pointA.position.y,
			};
			return point;
		},
		isVisible: function(){
			//TODO properly implement this function
			if((this.bone.pointB.score>0.6) && (this.bone.pointA.score>0.6)){
				return 'isVisible'
			}

		},
		styleObject: function(){
			return {
				'display': this.isVisible,
				'cursor': 'pointer',
			}
		},
	},
	methods: {
		displayTextpath(event){
			this.textPathIsVisible = 'visible';
			this.$emit('createBaseline', this);
		},
	},
})	

Vue.component('baseline', {
	template:`
		<svg class="baseline">
			<path 
				:d="pathString"
				:id="'baseline'+index" fill="transparent" stroke="black"/>
			<text width="500">
	      		<textPath 
	      			class="textPath"
	      			:style="styleBaseline"
	      			:xlink:href="'#baseline'+index">
	        		{{text}}
	      		</textPath>
	    	</text>
	    </svg>
	`,
	props: ['baseline', 'index'],
	data(){
		return {
			sharedState: store.state,
			baselineIsVisible: 'visible',
			isActiveBaseline: undefined,
			text: 'new text that is a bit long to see how we can get it to adjust',
			id: undefined
		}
	},
	computed: {
		styleBaseline: function(){
			return{
				'fill': 'white',
				'visibility': this.baselineIsVisible,
				'text-anchor': 'start',
				'stroke-width': '0',
				'x': '40',
				'y': '60',
			}
		},
		pathString: function(){
			if(this.isActiveBaseline != undefined){
				var string = '' ;
				string += 'M ';
				string += this.$parent.$refs['person'+this.baseline.personindex+this.baseline.points[0]][0].posX;
				string += ' ';
				string += this.$parent.$refs['person'+this.baseline.personindex+this.baseline.points[0]][0].posY;
				for(i=1; i<this.baseline.points.length; i++){
					//we get the equivalent keypoint in tableauTo
					var refName = 'person'+this.baseline.personindex+this.baseline.points[i];
					var keypointTo = this.$parent.$refs[refName][0];
					string += ' L ';
					string += keypointTo.posX;
					string += ' ';
					string += keypointTo.posY;
				};
				return string
			}
		}
	},
	mounted: function () {
		//we tell the baseline to look at the corresponding baseline in tableau
		this.isActiveBaseline = this.sharedState.baselineCounter-1;
	},
})


